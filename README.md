# Platinum Warp Tracker

A Tracker and street-map style navigator for the Pokemon Platinum Warp Randomiser. 

## Planned Features:

- [ ] Allow building a graph of Warp connections, both manually and automatically.
- [ ] Pre-made subgraphs for each hub, including conditions on traversing hubs based on HMs, story flags, items, or just one-way ledges.
- [ ] [DeSmuME](https://desmume.org) [Lua](https://www.lua.org) script to automatically display the warps for the current map, and automatically add new warps as they are found.
- [ ] Ability to load and view the warp graph from a ROM (.nds file) without starting the game
- [ ] Navigation assistant; choose a start (or auto-detect with Lua script) and a destination and be shown the shortest path.
        Optional spoiler mode to load warps from the ROM and ignore whether you've found the destination before.
- [ ] Ability to verify if a Randomised ROM is completeable or not.

## What does it work with?

This tool was intended for use with the [Universal Warp Randomiser for Pokemon](https://www.youtube.com/watch?v=LzRC6zTr5go) made by AtSign, XLuma, and Turtleisaac, and commisioned by PointCrow, which can be found for free on PointCrow's Discord server.

It will probably work fine with other Pokemon Platinum entrance randomisers, but it won't be tested with them.
