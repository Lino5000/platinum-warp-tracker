mod graph;
use graph::*;

fn main() {
    let mut graph = Graph::default();
    
    let a = graph.add_node(NodeType::Warp);
    let b = graph.add_node(NodeType::Warp);

    graph.add_edge(a, b, vec![], None);

    let a = graph.add_node(NodeType::Transition);
    let b = graph.add_node(NodeType::Warp);

    graph.add_edge(a, b, vec![], Some(true));

    let a = graph.add_node(NodeType::Warp);
    let b = graph.add_node(NodeType::Internal);

    graph.add_edge(a, b, vec![], Some(false));

    println!("{}", graph.dump_graphviz());
}
