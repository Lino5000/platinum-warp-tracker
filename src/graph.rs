use std::{
    rc::{ Rc, Weak },
    cell::RefCell,
    fmt::{ format }
};

#[derive(Debug, Clone, PartialEq)]
pub enum NodeType {
    Warp,
    Transition,
    Internal
}

#[derive(Debug, Clone, PartialEq)]
pub struct Node {
    id: usize,
    nodetype: NodeType,
    edges: Vec<Rc<RefCell<Edge>>>,
}

impl Node {
    fn new(id: usize, nodetype: NodeType) -> Self {
        Self {
            id,
            nodetype,
            edges: vec![]
        }
    }

    fn graphviz(&self) -> String {
        format(format_args!("{}{}", self.id, match self.nodetype {
            NodeType::Warp => " [shape=box]",
            NodeType::Transition => "",
            NodeType::Internal => " [xlabel=\"\\N\", shape=point]"
        }))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum EdgeCondition {
    Hm(u8),
    Gym(u8),
    StoryFlag(u8),
}

#[derive(Debug, Clone)]
pub struct Edge {
    conditions: Vec<EdgeCondition>,
    oneway: Option<bool>, // true => start -> end, false => end -> start
    nodes: (Weak<RefCell<Node>>, Weak<RefCell<Node>>),
}

impl Edge {
    fn new(start: Rc<RefCell<Node>>, end: Rc<RefCell<Node>>) -> Self {
        Self {
            conditions: vec![],
            oneway: None,
            nodes: (Rc::downgrade(&start), Rc::downgrade(&end))
        }
    }

    fn graphviz(&self) -> String {
        if let (Some(start), Some(end)) = (self.nodes.0.upgrade(), self.nodes.1.upgrade()) {
            format(format_args!("{} -> {}{}", start.borrow().id, end.borrow().id, {
                if self.conditions.len() == 0 && self.oneway == None {
                    " [arrowhead=\"none\", arrowtail=\"none\"]".to_owned()
                } else {
                    let mut label = String::new();
                    for cond in self.conditions.iter() {
                        match cond {
                            EdgeCondition::Hm(id) =>
                                label.push_str(&format(format_args!("HM-{:02} ", id))),
                            EdgeCondition::Gym(id) =>
                                label.push_str(&format(format_args!("Gym {} ", id))),
                            EdgeCondition::StoryFlag(id) =>
                                label.push_str(&format(format_args!("Story Flag {} ", id))),
                        };
                    }

                    format(format_args!(" [label=\"{}\", {}]",
                            label,
                            match self.oneway {
                                Some(true) => "arrowhead=normal",
                                Some(false) => "arrowtail=normal, dir=back",
                                None => "arrowhead=none"
                            }))
                }
            }))
        } else {
            "".to_owned()
        }
    }
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        self.conditions == other.conditions && {
            let (self_start_ref, self_end_ref) = self.nodes.clone();
            if let (Some(self_start), Some(self_end)) =
                (self_start_ref.upgrade(), self_end_ref.upgrade()) {
                    let (other_start_ref, other_end_ref) = other.nodes.clone();
                    if let (Some(other_start), Some(other_end)) =
                        (other_start_ref.upgrade(), other_end_ref.upgrade()) {
                            (self_start == other_start && self_end == other_end) ||
                                (self_start == other_end && self_end == other_start)
                        } else {
                            false
                        }
                } else {
                    false
                }
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Graph {
    nodes: Vec<Rc<RefCell<Node>>>,
    edges: Vec<Rc<RefCell<Edge>>>
}

impl Graph {
    pub fn add_node(&mut self, nodetype: NodeType) -> Rc<RefCell<Node>> {
        let node = Rc::new(RefCell::new(Node::new(self.nodes.len(), nodetype)));
        self.nodes.push(node.clone());
        node
    }

    pub fn add_edge(&mut self, start: Rc<RefCell<Node>>, end: Rc<RefCell<Node>>,
        conditions: Vec<EdgeCondition>, oneway: Option<bool>) -> Rc<RefCell<Edge>> {

        let edge = Rc::new(RefCell::new(Edge::new(start.clone(), end.clone())));
        { // restrict the scope of the dynamic borrow, just to be safe
            let mut edge = edge.borrow_mut();
            edge.conditions = conditions;
            edge.oneway = oneway;
        }
        self.edges.push(edge.clone());
        { // restrict the scope of the dynamic borrow, just to be safe
            start.borrow_mut().edges.push(edge.clone());
            end.borrow_mut().edges.push(edge.clone());
        }
        edge
    }

    pub fn dump_graphviz(&self) -> String {
        let mut graphviz = "digraph {\n".to_owned();
        for node in self.nodes.iter() {
            graphviz.push('\t');
            graphviz.push_str(&node.borrow().graphviz());
            graphviz.push('\n');
        }
        for edge in self.edges.iter() {
            graphviz.push('\t');
            graphviz.push_str(&edge.borrow().graphviz());
            graphviz.push('\n');
        }

        graphviz.push('}');
        graphviz
    }
}
